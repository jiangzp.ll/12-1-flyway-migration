create table contract (
    id int(11) not null auto_increment primary key ,
    name varchar(128) not null,
    branchId int(11) not null ,
    foreign key (branchId) references branch(id)
);
