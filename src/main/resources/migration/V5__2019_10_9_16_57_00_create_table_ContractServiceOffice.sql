create table contract_service_office (
    id int(11) not null auto_increment primary key ,
    office_id int(11) not null ,
    contract_id int(11) not null ,
    staff_id int(11) not null ,
    foreign key (office_id) references office(id),
    foreign key (contract_id) references contract(id),
    foreign key (staff_id) references staff(id)
);
