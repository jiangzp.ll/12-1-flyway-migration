create table branch (
    id int(11) not null auto_increment primary key ,
    clientId int(11) not null ,
    name varchar(128) not null,
    foreign key (clientId) references client(id)
);
