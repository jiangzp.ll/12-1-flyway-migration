create table staff (
    id int(11) not null auto_increment primary key ,
    officeId int(11) not null,
    first_name varchar (30) not null ,
    last_name varchar (30) not null ,
    foreign key (officeId) references office(id)
);
